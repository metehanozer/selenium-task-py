import pytest

from selenium.webdriver import Chrome

from pages.home_page import HomePage
from pages.login_page import LoginPage
from pages.product_page import ProductPage
from pages.result_page import ResultPage
from pages.wishlist_page import WishlistPage
from utils.helper import read_config


# chrome browser
@pytest.fixture
def browser():
    # Initialize ChromeDriver
    driver = Chrome()

    # Wait implicitly for elements to be ready before attempting interactions
    driver.implicitly_wait(10)

    driver.maximize_window()

    # Return the driver object at the end of setup
    yield driver

    # For cleanup, quit the driver
    driver.quit()


# test data
@pytest.fixture
def base():
    result = read_config('resources/test_data.ini', 'base')
    return result


def test_basic_duckduckgo_search(browser, base):

    # 1-  http://www.amazon.com sitesine gelecek ve anasayfanin acildigini onaylayacak,
    home_page = HomePage(browser)
    # home_page.load()
    home_page.load_url(base)
    assert home_page.nav_logo_link.get_attribute_to('aria-label').strip() == 'Amazon.de'

    # 2- Login ekranini acip, bir kullanici ile login olacak
    home_page.logout_hover.to_hover()
    home_page.signin_link.to_click()
    login_page = LoginPage(browser)
    login_page.email_input.to_clear()
    login_page.email_input.to_send_keys(base['email'])
    login_page.continue_button.to_click()
    login_page.password_input.to_clear()
    login_page.password_input.to_send_keys(base['password'])
    login_page.signin_button.to_click()

    # 3- Ekranin ustundeki Search alanina 'samsung' yazip Ara butonuna tiklayacak,
    home_page.search_input.to_send_keys(base['keyword'])
    home_page.search_button.to_click()

    # 4- Gelen sayfada samsung icin sonuc bulundugunu onaylayacak,
    result_page = ResultPage(browser)
    assert result_page.searhed_keyword.get_attribute_to('textContent').strip() == '"{}"'.format(base['keyword'])

    # 5- Arama sonuclarindan 2. sayfaya tiklayacak ve acilan sayfada 2. sayfanin su an gosterimde oldugunu onaylayacak,
    result_page.next_page_button.to_click()
    assert result_page.selected_page.get_attribute_to('textContent').strip() == '2'

    # 6- Ustten 3. urunun icindeki 'Add to List' butonuna tiklayacak,
    search_results = result_page.result_list.finds_to()
    search_results[2].click()

    # 7- Ekranin en ustundeki 'List' linkine tiklayacak içerisinden Wish listi seçecek,
    product_page = ProductPage(browser)
    selected_product = product_page.product_title.get_attribute_to('textContent').strip()
    product_page.add_wish_list.to_click()
    product_page.show_wish_list.to_click()

    # 8- Acilan sayfada bir onceki sayfada izlemeye alinmis urunun bulundugunu onaylayacak,
    wishlist_page = WishlistPage(browser)
    assert wishlist_page.first_product.get_attribute_to('title').strip() == selected_product

    # 9- Favorilere alinan bu urunun yanindaki 'Delete' butonuna basarak, favorilerimden cikaracak,
    wishlist_page.remove_all_product_from_wishlist()

    # 10- Sayfada bu urunun artik favorilere alinmadigini onaylayacak.
    assert wishlist_page.empty_message.get_attribute_to('textContent').strip() == 'Bu listede 0 ürün var'
