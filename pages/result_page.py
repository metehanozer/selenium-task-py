from selenium.webdriver.common.by import By

from pages.base_element import BaseElement
from pages.base_page import BasePage


class ResultPage(BasePage):

    @property
    def searhed_keyword(self):
        locator = (By.CSS_SELECTOR, ".a-color-state.a-text-bold")
        return BaseElement(driver=self.browser, by=locator[0], value=locator[1])

    @property
    def next_page_button(self):
        locator = (By.CSS_SELECTOR, ".a-last > a")
        return BaseElement(driver=self.browser, by=locator[0], value=locator[1])

    @property
    def selected_page(self):
        locator = (By.CSS_SELECTOR, ".a-selected > a")
        return BaseElement(driver=self.browser, by=locator[0], value=locator[1])

    @property
    def result_list(self):
        locator = (By.XPATH, "//div[@class='s-result-list s-search-results sg-row']/div/div/span/div/div/div[2]/div/div/div/span/a")
        return BaseElement(driver=self.browser, by=locator[0], value=locator[1])