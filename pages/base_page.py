
class BasePage(object):

    url = None

    def __init__(self, browser):
        self.browser = browser

    def load(self):
        self.browser.get(self.url)

    def load_url(self, base):
        self.browser.get(base['url'])

    def refresh_page(self):
        self.browser.refresh()
