from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys


class HomePage2:
    URL = 'https://www.amazon.com.tr/'

    SEARCH_INPUT = (By.ID, 'search_form_input_homepage')
    NAV_LOGO_LINK = (By.CSS_SELECTOR, 'div#nav-logo > .nav-logo-link')

    def __init__(self, browser):
        self.browser = browser

    def load(self):
        self.browser.get(self.URL)

    def get_nav_text(self):
        nav_logo_link = self.browser.find_element(*self.NAV_LOGO_LINK)
        return nav_logo_link.get_attribute('aria-label')

    def search(self, phrase):
        search_input = self.browser.find_element(*self.SEARCH_INPUT)
        search_input.send_keys(phrase + Keys.RETURN)
