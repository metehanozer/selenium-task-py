from selenium.webdriver.common.by import By

from pages.base_element import BaseElement
from pages.base_page import BasePage


class HomePage(BasePage):

    url = 'https://www.amazon.com.tr/'

    @property
    def nav_logo_link(self):
        locator = (By.CSS_SELECTOR, "div#nav-logo > .nav-logo-link")
        return BaseElement(driver=self.browser, by=locator[0], value=locator[1])

    @property
    def logout_hover(self):
        locator = (By.CSS_SELECTOR, "a#nav-link-accountList")
        return BaseElement(driver=self.browser, by=locator[0], value=locator[1])

    @property
    def signin_link(self):
        locator = (By.CSS_SELECTOR, "div#nav-flyout-ya-signin  .nav-action-inner")
        return BaseElement(driver=self.browser, by=locator[0], value=locator[1])

    @property
    def search_input(self):
        locator = (By.ID, "twotabsearchtextbox")
        return BaseElement(driver=self.browser, by=locator[0], value=locator[1])

    @property
    def search_button(self):
        locator = (By.CSS_SELECTOR, "[value='Git']")
        return BaseElement(driver=self.browser, by=locator[0], value=locator[1])