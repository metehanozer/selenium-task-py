from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException

from pages.base_element import BaseElement
from pages.base_page import BasePage


class WishlistPage(BasePage):

    @property
    def first_product(self):
        locator = (By.XPATH, "//div[@class='a-row a-size-small']/h3/a")
        return BaseElement(driver=self.browser, by=locator[0], value=locator[1])

    @property
    def remove_product_link(self):
        locator = (By.LINK_TEXT, "Ürünü kaldır")
        return BaseElement(driver=self.browser, by=locator[0], value=locator[1])

    @property
    def empty_message(self):
        locator = (By.CSS_SELECTOR, ".a-color-base.a-size-medium")
        return BaseElement(driver=self.browser, by=locator[0], value=locator[1])

    # Daha önceden eklenmiş ürünler olabilir diye bütün ürünler siliniyor.
    def remove_all_product_from_wishlist(self):
        while True:
            try:
                self.remove_product_link.to_click()
                self.refresh_page()
                print("wish list dolu, ürün siliniyor...")

            except TimeoutException:
                print("wish list boş...")
                break
