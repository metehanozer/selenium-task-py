from selenium.webdriver import ActionChains
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec

WAIT_TIME = 10


class BaseElement(object):
    def __init__(self, driver, by, value):
        self.driver = driver
        self.by = by
        self.value = value
        self.locator = (self.by, self.value)

        self.element = None

        self.find_to()

    def find_to(self):
        element = WebDriverWait(self.driver, WAIT_TIME).until(ec.visibility_of_element_located(locator=self.locator))
        self.element = element
        return None

    def to_clear(self):
        self.element.clear()
        return None

    def to_send_keys(self, text):
        self.element.send_keys(text)
        return None

    def to_click(self):
        element = WebDriverWait(self.driver, WAIT_TIME).until(ec.element_to_be_clickable(locator=self.locator))
        element.click()
        return None

    def to_hover(self):
        element = WebDriverWait(self.driver, WAIT_TIME).until(ec.element_to_be_clickable(locator=self.locator))
        hover = ActionChains(self.driver).move_to_element(element)
        hover.perform()

    def get_attribute_to(self, attribute_name):
        attribute = self.element.get_attribute(attribute_name)
        return attribute

    @property
    def get_text_to(self):
        text = self.element.text
        return text

    def is_visible(self):
        if WebDriverWait(self.driver, WAIT_TIME).until(ec.visibility_of(element=self.element)):
            return True
        else:
            return False

    def is_displayed(self):
        if self.element.is_displayed():
            return True
        else:
            return False

    def finds_to(self):
        if self.element is not None:
            return WebDriverWait(self.element, WAIT_TIME).until(ec.visibility_of_all_elements_located(locator=self.locator))
        else:
            return None