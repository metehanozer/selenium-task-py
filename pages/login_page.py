from selenium.webdriver.common.by import By

from pages.base_element import BaseElement
from pages.base_page import BasePage


class LoginPage(BasePage):

    @property
    def email_input(self):
        locator = (By.ID, "ap_email")
        return BaseElement(driver=self.browser, by=locator[0], value=locator[1])

    @property
    def continue_button(self):
        locator = (By.CSS_SELECTOR, "input#continue")
        return BaseElement(driver=self.browser, by=locator[0], value=locator[1])

    @property
    def password_input(self):
        locator = (By.ID, "ap_password")
        return BaseElement(driver=self.browser, by=locator[0], value=locator[1])

    @property
    def signin_button(self):
        locator = (By.CSS_SELECTOR, "input#signInSubmit")
        return BaseElement(driver=self.browser, by=locator[0], value=locator[1])