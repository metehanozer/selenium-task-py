from selenium.webdriver.common.by import By

from pages.base_element import BaseElement
from pages.base_page import BasePage


class ProductPage(BasePage):

    @property
    def product_title(self):
        locator = (By.CSS_SELECTOR, "span#productTitle")
        return BaseElement(driver=self.browser, by=locator[0], value=locator[1])

    @property
    def add_wish_list(self):
        locator = (By.CSS_SELECTOR, "input#add-to-wishlist-button-submit")
        return BaseElement(driver=self.browser, by=locator[0], value=locator[1])

    @property
    def show_wish_list(self):
        locator = (By.CSS_SELECTOR, "a#WLHUC_viewlist  .w-button-text")
        return BaseElement(driver=self.browser, by=locator[0], value=locator[1])