# 🎁 Selenium Task Python

Python pytest ile selenium test projesi...


### Ön Koşullar

test_data.ini dosyasına amazon.com.tr için geçerli email ve password bilgileri girilmelidir.\
Aşağıdaki komut ile gerekli python kütüphaneleri yüklenmelidir.
~~~
pipenv install
~~~


### Test

Testi çalıştırmak için...
~~~
pipenv run python -m pytest
~~~
